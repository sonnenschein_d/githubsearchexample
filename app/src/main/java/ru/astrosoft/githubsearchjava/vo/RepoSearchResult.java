package ru.astrosoft.githubsearchjava.vo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import ru.astrosoft.githubsearchjava.db.SearchTypeConverters;

/**
 * Created by Ekaterina Trofimova on 31/03/2018.
 */
@Entity
@TypeConverters(SearchTypeConverters.class)
public class RepoSearchResult {
    @PrimaryKey
    @NonNull
    public final String query;
    public final List<Integer> repoIds;
    public final int totalCount;
    public final long timestamp;
    @Nullable
    public final Integer next;

    public RepoSearchResult(@NonNull String query, List<Integer> repoIds, int totalCount,
                            long timestamp, @Nullable Integer next) {
        this.query = query;
        this.repoIds = repoIds;
        this.totalCount = totalCount;
        this.timestamp = timestamp;
        this.next = next;
    }
}
