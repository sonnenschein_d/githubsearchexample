package ru.astrosoft.githubsearchjava.util;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Ekaterina Trofimova on 01/04/2018.
 */
@Singleton
public class AppExecutors {

    private final Executor networkIO;

    public AppExecutors(Executor networkIO) {
        this.networkIO = networkIO;
    }

    @Inject
    public AppExecutors(){
        this(Executors.newFixedThreadPool(3));
    }

    public Executor getNetworkIO() {
        return networkIO;
    }
}
