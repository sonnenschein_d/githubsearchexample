package ru.astrosoft.githubsearchjava.util;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public abstract class DisposingDaggerActivity extends DaggerAppCompatActivity {

    @Inject
    protected DisposableManager disposableManager;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposableManager.dispose();
    }
}
