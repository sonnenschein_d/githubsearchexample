package ru.astrosoft.githubsearchjava.util;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

@Singleton
public class DisposableManager {

    private CompositeDisposable disposable = null;

    @Inject
    public DisposableManager(){}

    public void add(Disposable disposable) {
        getCompositeDisposable().add(disposable);
    }

    public void dispose() {
        getCompositeDisposable().dispose();
    }

    private CompositeDisposable getCompositeDisposable() {
        if (disposable == null || disposable.isDisposed()) {
            disposable = new CompositeDisposable();
        }
        return disposable;
    }
}
