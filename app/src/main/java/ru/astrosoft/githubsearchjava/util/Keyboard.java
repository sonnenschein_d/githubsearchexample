package ru.astrosoft.githubsearchjava.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Ekaterina.Trofimova on 21-Apr-17.
 */

public final class Keyboard {

    private Keyboard(){throw new IllegalAccessError();}

    public static void hide(Activity activity){
        View focused = activity.getCurrentFocus();
        if (focused != null){
            focused.clearFocus();
            hide(activity, focused);
        }
    }

    private static void hide(Context context, View visible){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(visible.getWindowToken(), 0);
    }

}
