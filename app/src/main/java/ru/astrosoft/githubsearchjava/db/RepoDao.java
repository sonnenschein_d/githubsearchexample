package ru.astrosoft.githubsearchjava.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

import io.reactivex.Flowable;
import ru.astrosoft.githubsearchjava.vo.Repo;
import ru.astrosoft.githubsearchjava.vo.RepoSearchResult;

/**
 * Created by Ekaterina Trofimova on 31/03/2018.
 */
@Dao
public abstract class RepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertRepos(List<Repo> repositories);

    @Query("SELECT * FROM RepoSearchResult WHERE query = :query AND timestamp > :oldest")
    public abstract Flowable<List<RepoSearchResult>> search(String query, long oldest);

    @Query("SELECT * FROM Repo WHERE id in (:repoIds)")
    public abstract Flowable<List<Repo>> loadByIds(List<Integer> repoIds);

    @Query("SELECT * FROM RepoSearchResult WHERE query = :query")
    public abstract RepoSearchResult searchSync(String query);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(RepoSearchResult result);

    @Transaction
    public void insert(RepoSearchResult result, List<Repo> repos) {
        insert(result);
        insertRepos(repos);
    }
}
