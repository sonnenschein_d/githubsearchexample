package ru.astrosoft.githubsearchjava.db;

import java.util.Date;

import ru.astrosoft.githubsearchjava.api.ApiConstants;

/**
 * Created by Ekaterina Trofimova on 31/03/2018.
 */
public final class DbUtil {

    private DbUtil(){throw new IllegalAccessError();}

    public static long getOldestValid(){
        return new Date().getTime() - ApiConstants.VALID_TIME_UNIT.toMillis(ApiConstants.VALID_TIME);
    }
}
