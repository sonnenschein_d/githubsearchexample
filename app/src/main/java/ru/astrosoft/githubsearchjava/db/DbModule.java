package ru.astrosoft.githubsearchjava.db;

import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ekaterina Trofimova on 31/03/2018.
 */
@Module
public class DbModule {

    @Singleton
    @Provides
    GithubDb provideDb(Context context) {
        return Room.databaseBuilder(context, GithubDb.class, "github.db").build();
    }

    @Singleton
    @Provides
    RepoDao provideRepoDao(GithubDb db) {
        return db.repoDao();
    }

}
