package ru.astrosoft.githubsearchjava.repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import ru.astrosoft.githubsearchjava.api.ApiConstants;
import ru.astrosoft.githubsearchjava.api.ApiResponse;
import ru.astrosoft.githubsearchjava.api.GithubService;
import ru.astrosoft.githubsearchjava.api.RepoSearchResponse;
import ru.astrosoft.githubsearchjava.db.RepoDao;
import ru.astrosoft.githubsearchjava.vo.RepoSearchResult;
import timber.log.Timber;

/**
 * Created by Ekaterina Trofimova on 01/04/2018.
 */
public class LoadMoreTask implements Runnable {

    private final String query;
    private final RepoDao dao;
    private final GithubService githubService;

    private BehaviorSubject<Boolean> loading = BehaviorSubject.create();

    public LoadMoreTask(String query, RepoDao dao, GithubService githubService) {
        this.query = query;
        this.dao = dao;
        this.githubService = githubService;
    }

    private List<Integer> merge(List<Integer> list1, List<Integer> list2){
        List<Integer> merged = new ArrayList<>(list1);
        merged.addAll(list2);
        return merged;
    }

    @Override
    public void run() {
        RepoSearchResult res = dao.searchSync(query);
        if (res == null || res.next == null){
            loading.onComplete();
            return;
        }
        try {
            loading.onNext(true);
            ApiResponse<RepoSearchResponse> response = new ApiResponse<RepoSearchResponse>(
                    githubService
                    .searchRepos(query, res.next, ApiConstants.SORT_STARS)
                    .execute()
            );
            if (!response.isSuccessful() || response.body == null){
                return;
            }
            List<Integer> updatedRepoIds = merge(res.repoIds, response.body.getRepoIds());
            RepoSearchResult updated = new RepoSearchResult(
                    query,
                    updatedRepoIds,
                    updatedRepoIds.size(),
                    res.timestamp,
                    response.getNextPage()
            );
            dao.insert(updated, response.body.getItems());

        } catch (IOException e) {
            Timber.e(e);
        } finally {
            loading.onNext(false);
            loading.onComplete();
        }
    }

    public Observable<Boolean> isLoading(){
        return loading;
    }
}
