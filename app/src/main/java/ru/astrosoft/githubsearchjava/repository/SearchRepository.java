package ru.astrosoft.githubsearchjava.repository;

import android.annotation.SuppressLint;
import android.util.SparseIntArray;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import ru.astrosoft.githubsearchjava.api.ApiConstants;
import ru.astrosoft.githubsearchjava.api.ApiResponse;
import ru.astrosoft.githubsearchjava.api.GithubService;
import ru.astrosoft.githubsearchjava.api.RepoSearchResponse;
import ru.astrosoft.githubsearchjava.db.DbUtil;
import ru.astrosoft.githubsearchjava.db.RepoDao;
import ru.astrosoft.githubsearchjava.util.AppExecutors;
import ru.astrosoft.githubsearchjava.vo.Repo;
import ru.astrosoft.githubsearchjava.vo.RepoSearchResult;
import timber.log.Timber;

/**
 * Created by Ekaterina Trofimova on 31/03/2018.
 */
@Singleton
public class SearchRepository {

    private GithubService githubService;
    private RepoDao dao;
    private AppExecutors executors;

    @Inject
    public SearchRepository(GithubService githubService, RepoDao dao, AppExecutors executors) {
        this.dao = dao;
        this.githubService = githubService;
        this.executors = executors;
    }

    private Flowable<List<Repo>> empty() {
        return Flowable.just(Collections.emptyList());
    }

    public Observable<Boolean> loadMore(String query){
        LoadMoreTask task = new LoadMoreTask(query, dao, githubService);
        executors.getNetworkIO().execute(task);
        return task.isLoading();
    }

    public Flowable<List<Repo>> getSearchResult(String query) {
        Timber.d("get search results for query = " + query);
        return dao.search(query, DbUtil.getOldestValid())
                .filter(list -> {
                    if (list.isEmpty()){
                        //no valid data in db yet. send request to api
                        search(query);
                    }
                    return !list.isEmpty();
                })
                .switchMap(list -> {
                    //TODO: called to many times. fix it
                    Timber.d("switchMap search results for query = " + query);
                    RepoSearchResult item = list.get(0);
                    List<Integer> ids = item.repoIds;
                    //we have already searched for this query, but found nothing
                    if (ids == null || ids.isEmpty()) {
                        return empty();
                    }

                    //return found repos
                    return dao.loadByIds(ids)
                            .map(loaded -> {
                                SparseIntArray order = new SparseIntArray();
                                int index = 0;
                                for (Integer repoId : ids) {
                                    order.put(repoId, index++);
                                }
                                Collections.sort(loaded, (r1, r2) ->{
                                    int pos1 = order.get(r1.id);
                                    int pos2 = order.get(r2.id);
                                    return pos1 - pos2;
                                });
                                return loaded;
                            });
                });
    }

    @SuppressLint("CheckResult")
    private void search(String query) {
        githubService.searchRepos(query, ApiConstants.SORT_STARS)
                .map(ApiResponse::new)
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {
                    Timber.d("Received github search results for query = " + query);
                    if (response.isSuccessful()) {
                        save(response, query);
                    } else {
                        processError(response);
                    }
                });

    }

    private void processError(ApiResponse<RepoSearchResponse> response) {
        //TODO: implement ru.astrosoft.githubsearchjava.repository.SearchRepository processError

    }

    private void save(ApiResponse<RepoSearchResponse> response, String query) {
        RepoSearchResult res = new RepoSearchResult(
                query,
                response.body.getRepoIds(),
                response.body.getTotal(),
                new Date().getTime(),
                response.getNextPage()
        );
        dao.insert(res, response.body.getItems());
    }

}
