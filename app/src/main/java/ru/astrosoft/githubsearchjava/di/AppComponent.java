package ru.astrosoft.githubsearchjava.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;
import ru.astrosoft.githubsearchjava.App;
import ru.astrosoft.githubsearchjava.api.NetworkModule;
import ru.astrosoft.githubsearchjava.db.DbModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class,
        NetworkModule.class,
        DbModule.class,
        ViewModelModule.class})
public interface AppComponent extends AndroidInjector<DaggerApplication> {

    void inject(App app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

}
