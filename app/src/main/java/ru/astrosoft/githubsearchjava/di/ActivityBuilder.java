package ru.astrosoft.githubsearchjava.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.astrosoft.githubsearchjava.ui.reposearch.RepoSearchActivity;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = RepoSearchActivityModule.class)
    abstract RepoSearchActivity bindRepoSearchActivity();
}
