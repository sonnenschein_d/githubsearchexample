package ru.astrosoft.githubsearchjava.ui.reposearch;

import android.arch.lifecycle.ViewModel;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import ru.astrosoft.githubsearchjava.repository.SearchRepository;
import ru.astrosoft.githubsearchjava.vo.Repo;
import timber.log.Timber;

public class RepoSearchViewModel extends ViewModel {

    private Subject<String> query = BehaviorSubject.create();
    private SearchRepository repository;

    private Observable<List<Repo>> repos;
    private Subject<Boolean> isLoading = BehaviorSubject.create();

    private Subject<String> loadMore = PublishSubject.create();

    private Observable<Boolean> loadMoreState;

    @Inject
    public RepoSearchViewModel(SearchRepository repository) {
        this.repository = repository;

        this.repos = query
                .observeOn(Schedulers.io())
                .switchMap(query -> repository.getSearchResult(query).toObservable())
                .doOnNext(x -> {
                    isLoading.onNext(false);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());

        this.loadMoreState = loadMore
                .debounce(500, TimeUnit.MILLISECONDS)
                .switchMap(query -> {
                    Timber.d("loading more for query = " + query);
                    return repository.loadMore(query);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());

    }

    public void setQuery(String query) {
        String processed = query.toLowerCase().trim();
        if (processed.isEmpty()) {
            return;
        }
        this.isLoading.onNext(true);
        this.query.onNext(query.toLowerCase().trim());
    }

    public Observable<List<Repo>> getRepos() {
        return repos;
    }

    public Observable<Boolean> getLoadMoreState() {
        return loadMoreState;
    }

    public Observable<Boolean> getLoading() {
        return isLoading
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    public void loadMore(){
        Timber.d("Load more is called");
        query.take(1).subscribe(query -> {
            loadMore.onNext(query);
        }).dispose();
    }

}
