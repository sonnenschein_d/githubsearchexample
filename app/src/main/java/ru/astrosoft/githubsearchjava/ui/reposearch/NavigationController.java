package ru.astrosoft.githubsearchjava.ui.reposearch;

import android.support.v4.app.FragmentManager;

import javax.inject.Inject;

import ru.astrosoft.githubsearchjava.R;
import ru.astrosoft.githubsearchjava.vo.Repo;
import timber.log.Timber;

/**
 * Created by Ekaterina Trofimova on 29/03/2018.
 */
public class NavigationController {

    private final int containerId;
    private final FragmentManager fragmentManager;

    @Inject
    public NavigationController(RepoSearchActivity mainActivity) {
        this.containerId = R.id.container;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
    }

    public void navigateToRepoDetails(Repo repo){
        Timber.d("navigate to " + repo.toString());
        //TODO: implement
    }
}
