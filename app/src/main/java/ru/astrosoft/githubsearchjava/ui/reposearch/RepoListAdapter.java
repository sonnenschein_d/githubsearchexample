/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.astrosoft.githubsearchjava.ui.reposearch;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import ru.astrosoft.githubsearchjava.R;
import ru.astrosoft.githubsearchjava.databinding.ItemRepoBinding;
import ru.astrosoft.githubsearchjava.ui.common.DataBoundListAdapter;
import ru.astrosoft.githubsearchjava.util.Objects;
import ru.astrosoft.githubsearchjava.vo.Repo;

public class RepoListAdapter extends DataBoundListAdapter<Repo, ItemRepoBinding> {
    private final RepoClickCallback repoClickCallback;

    public RepoListAdapter(RepoClickCallback repoClickCallback) {
        this.repoClickCallback = repoClickCallback;
    }

    @Override
    protected ItemRepoBinding createBinding(ViewGroup parent) {
        ItemRepoBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_repo,
                        parent, false);
        binding.getRoot().setOnClickListener(v -> {
            Repo repo = binding.getRepo();
            if (repo != null && repoClickCallback != null) {
                repoClickCallback.onClick(repo);
            }
        });
        return binding;
    }

    @Override
    protected void bind(ItemRepoBinding binding, Repo item) {
        binding.setRepo(item);
    }

    @Override
    protected boolean areItemsTheSame(Repo oldItem, Repo newItem) {
        return Objects.equals(oldItem.owner, newItem.owner)
                && Objects.equals(oldItem.name, newItem.name);
    }

    @Override
    protected boolean areContentsTheSame(Repo oldItem, Repo newItem) {
        return Objects.equals(oldItem.description, newItem.description)
                && oldItem.stars == newItem.stars
                && Objects.equals(oldItem.language, newItem.language);
    }

    public interface RepoClickCallback {
        void onClick(Repo repo);
    }
}
