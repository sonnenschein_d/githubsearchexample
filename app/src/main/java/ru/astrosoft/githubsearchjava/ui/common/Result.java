package ru.astrosoft.githubsearchjava.ui.common;

import ru.astrosoft.githubsearchjava.api.ApiResponse;
@Deprecated
public class Result<T> {
    public final T data;
    public final String errorMsg;
    public final State state;

    public Result(T data, String errorMsg, State state) {
        this.data = data;
        this.errorMsg = errorMsg;
        this.state = state;
    }

    public enum State{
        SUCCESS,
        ERROR,
    }

    public static <T> Result<T> createFrom(ApiResponse<T> response){
        return new Result<T>(response.body,
                response.errorMessage,
                response.isSuccessful() ? State.SUCCESS : State.ERROR);
    }

}
