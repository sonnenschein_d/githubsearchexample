package ru.astrosoft.githubsearchjava.ui.reposearch;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.astrosoft.githubsearchjava.R;
import ru.astrosoft.githubsearchjava.databinding.ActivityRepoSearchBinding;
import ru.astrosoft.githubsearchjava.util.DisposingDaggerActivity;
import ru.astrosoft.githubsearchjava.util.Keyboard;
import ru.astrosoft.githubsearchjava.vo.Repo;
import timber.log.Timber;


public class RepoSearchActivity extends DisposingDaggerActivity {

    public static final String BUNDLE_RECYCLER_LAYOUT = "recycler_view_layout";

    @Inject
    NavigationController navigationController;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private RepoSearchViewModel viewModel;
    private ActivityRepoSearchBinding binding;
    private RepoListAdapter adapter;

    private Parcelable rvState;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, binding.content.rvRepos.getLayoutManager().onSaveInstanceState());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        rvState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
    }

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_repo_search);
        setSupportActionBar(binding.toolbar);

        adapter = new RepoListAdapter(repo -> navigationController.navigateToRepoDetails(repo));
        binding.content.rvRepos.setAdapter(adapter);

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RepoSearchViewModel.class);

        binding.content.rvRepos.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                int lastPosition = layoutManager
                        .findLastVisibleItemPosition();
                if (lastPosition == adapter.getItemCount() - 1) {
                    rvState = binding.content.rvRepos.getLayoutManager().onSaveInstanceState();
                    viewModel.loadMore();
                }
            }
        });

        viewModel.getRepos()
                .doOnSubscribe(disposableManager::add)
                .subscribe(repos -> {
                            Timber.d("Activity received search results = " + (repos == null ? 0 : repos.size()));
                            binding.setResultCount(repos == null ? 0 : repos.size());
                            adapter.replace(repos);
                            if (rvState != null){
                                binding.content.rvRepos.getLayoutManager().onRestoreInstanceState(rvState);
                            }
                            binding.executePendingBindings();
                            //TODO: process possible errors somehow
                        });

        viewModel.getLoading()
                .doOnSubscribe(disposableManager::add)
                .subscribe(loading -> {
                    binding.setIsLoading(loading);
                    binding.executePendingBindings();
                });

        viewModel.getLoadMoreState()
                .doOnSubscribe(disposableManager::add)
                .subscribe(isLoading ->{
                   binding.setLoadingMore(isLoading);
                   binding.executePendingBindings();
                });

    }

    private int lastVisibleItemPosition(){
        return ((LinearLayoutManager)binding.content.rvRepos.getLayoutManager())
                .findLastVisibleItemPosition();
    }

    private void showError(String msg){
        Snackbar mySnackbar = Snackbar.make(binding.getRoot(),
                msg, Snackbar.LENGTH_INDEFINITE);
        mySnackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_repo_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        setSearchListener(searchView);

        return true;
    }

    private void setSearchListener(SearchView searchView) {
        searchView.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Keyboard.hide(RepoSearchActivity.this);
                doSearch(searchView.getQuery().toString());
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void doSearch(String query) {
        viewModel.setQuery(query);
    }

}
