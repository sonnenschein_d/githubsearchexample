package ru.astrosoft.githubsearchjava;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import ru.astrosoft.githubsearchjava.di.AppComponent;
import ru.astrosoft.githubsearchjava.di.DaggerAppComponent;
import timber.log.Timber;
import timber.log.Timber.DebugTree;

public class App extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG){
            Timber.plant(new DebugTree());
        }
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }
}
