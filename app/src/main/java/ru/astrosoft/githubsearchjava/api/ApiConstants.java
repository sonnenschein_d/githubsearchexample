package ru.astrosoft.githubsearchjava.api;

import java.util.concurrent.TimeUnit;

public final class ApiConstants {

    private ApiConstants(){throw new IllegalAccessError();}

    public static final String SORT_STARS = "stars";
    public static final TimeUnit VALID_TIME_UNIT = TimeUnit.MINUTES;
    public static final int VALID_TIME = 10;
}
