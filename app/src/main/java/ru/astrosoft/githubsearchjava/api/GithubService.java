package ru.astrosoft.githubsearchjava.api;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GithubService {

    @GET("search/repositories")
    Observable<Response<RepoSearchResponse>> searchRepos(@Query("q") String query, @Query("sort") String sort);

    @GET("search/repositories")
    Call<RepoSearchResponse> searchRepos(@Query("q") String query, @Query("page") int page, @Query("sort") String sort);
}
